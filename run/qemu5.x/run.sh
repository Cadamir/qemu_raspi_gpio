#!/bin/bash

path=$(cd $(dirname $0); pwd)
cd $path

sshPort=2222
httpPort=8080
httpsPort=8888

maschine="raspi3"

append="rw earlyprintk loglevel=8 console=ttyAMA0,,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1"

dtb="../../dtb/bcm2710-rpi-3-b-plus.dtb"
kernel="../../kernel/kernel8.img"
image="../../image/2021-10-30-raspios-bullseye-armhf-lite.img"

ram="1G"
smp="4"

socket="tcp:127.0.0.1:2223"

qemu-system-aarch64 	\
	-M $maschine	\
	-append " $append "	\
	-dtb $dtb	\
	-kernel $kernel	\
	-sd $image	\
	-m $ram		\
	-smp $smp	\
	-serial stdio	\
	-usb		\
	-device usb-mouse	\
	-device usb-kbd	\
	-device usb-net,netdev=net0\
	-netdev user,id=net0,hostfwd=tcp::$sshPort-:22,hostfwd=tcp::$httpPort-:80,hostfwd=tcp::$httpsPort-:443
