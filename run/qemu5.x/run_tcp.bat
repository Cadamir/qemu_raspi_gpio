@echo off 

set sshPort=2222
set httpPort=8080
set httpsPort=8888

set maschine="raspi3"

set append="rw earlyprintk loglevel=8 console=ttyAMA0,,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1"

set dtb="./dtb/bcm2710-rpi-3-b-plus.dtb"
set kernel="./kernel/kernel8.img"
set image="./image/2021-10-30-raspios-bullseye-armhf-lite.img"

set ram="1G"
set smp="4"

set socket="tcp:127.0.0.1:2223"

qemu-system-aarch64 	^
	-M %maschine%	^
	-append "rw earlyprintk loglevel=8 console=ttyAMA0,,115200 dwc_otg.lpm_enable=0 root=/dev/mmcblk0p2 rootdelay=1"	^
	-dtb %dtb%	^
	-kernel %kernel%	^
	-sd %image%	^
	-m %ram%		^
	-smp %smp%	^
	-serial stdio	^
	-usb		^
	-device usb-mouse	^
	-device usb-kbd	^
	-device usb-net,netdev=net0 ^
	-netdev user,id=net0,hostfwd=tcp::%sshPort%-:22,hostfwd=tcp::%httpPort%-:80,hostfwd=tcp::%httpsPort%-:443	^
	-qtest %socket%
 
