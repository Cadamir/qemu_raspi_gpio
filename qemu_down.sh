#!/bin/bash

echo "Install git"
sudo apt-get install git

echo "Install ninja-build"
sudo apt-get install ninja-build

echo "Install build-essential"
sudo apt-get install build-essential

echo "Install zlib1g-dev"
sudo apt-get install zlib1g-dev

echo "Install pkg-config"
sudo apt-get install pkg-config

echo "Install libglib2.0-dev"
sudo apt-get install libglib2.0-dev

echo "Install binutils-dev"
sudo apt-get install binutils-dev

echo "Install libboost-all-dev"
sudo apt-get install libboost-all-dev

echo "Install autoconf"
sudo apt-get install autoconf

echo "Install libtool"
sudo apt-get install libtool

echo "Install libssl-dev"
sudo apt-get install libssl-dev

echo "Install libpixman-1-dev"
sudo apt-get install libpixman-1-dev

echo "Install libpython-dev"
sudo apt-get install libpython-dev

echo "Install libpython-dev"
sudo apt-get install libpython-dev

echo "Install python-pip"
sudo apt-get install python-pip

echo "Install python-capstone"
sudo apt-get install python-capstone

echo "Install virtualenv"
sudo apt-get install virtualenv

echo "Install qemu-system-data"
sudo apt-get install qemu-system-data


git clone "https://github.com/qemu/qemu.git"

cd qemu

mkdir build

cd build

../configure

make
