#!/bin/bash

if [ $# -eq 1 ]
then

path=$(cd $(dirname $0); pwd)
cd $path


str=$(fdisk -l "$1" | grep img1)

IFS=' '
read -ra ARR <<< "$str"

sudo mount -t auto -o loop,offset=$((${ARR[1]} * 512)) $1 ./sd/

sudo cp ./sd/kernel8.img ../kernel/
sudo cp ./sd/bcm2710-rpi-3-b-plus.dtb ../dtb/

sudo umount ./sd/
exit 0
fi

echo "usage: ./extract path/to/image"
echo "the image has to be a parameter"

