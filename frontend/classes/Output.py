##  Frontend-Imports
import threading
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

#######################################################################################################################
# Start Output

class Output(Gtk.Frame):
    def __init__(self, nr, color, parent):
        super().__init__()

        # parent
        self.parent = parent

        # nr of GPIO
        self.nr = nr
        super().set_label(str(self.nr))

        # choosen color
        self.color = color

        #bufferd leds(for later color change)
        self.grey = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDgrey.png", width=50, height=50, preserve_aspect_ratio=True)
        self.blue = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDblue.png", width=50, height=50, preserve_aspect_ratio=True)
        self.turquoise = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDturquoise.png", width=50, height=50, preserve_aspect_ratio=True)
        self.green = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDgreen.png", width=50, height=50, preserve_aspect_ratio=True)
        self.yellow = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDyellow.png", width=50, height=50, preserve_aspect_ratio=True)
        self.red = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDred.png", width=50, height=50, preserve_aspect_ratio=True)
        self.violett = pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename="./pictures/LEDviolett.png", width=50, height=50, preserve_aspect_ratio=True)

        # image 
        self.gled = Gtk.Image.new_from_pixbuf(self.grey)

        switcher = {
            "grey": self.grey,
            "blue": self.blue,
            "turquoise": self.turquoise,
            "green": self.green,
            "yellow": self.yellow,
            "red": self.red,
            "violett": self.violett
        }

        self.cled = Gtk.Image.new_from_pixbuf(switcher.get(self.color, self.blue))
        self.add(self.gled)
        self.color = False

        # Aktualisierungsthread
        x = threading.Thread(target=self.aktThread, daemon=True)
        x.start()

    def on(self):
        if self.color:
            return
        self.color = True
        self.remove(self.gled)
        self.add(self.cled)
        self.show_all()
    
    def off(self):
        if not self.color:
            return
        self.color = False
        self.remove(self.cled)
        self.add(self.gled)
        self.show_all()

    def aktThread(self):
        while True:
            time.sleep(1)
            state = self.parent.qemuManager.parse("get " + str(self.nr))
            if state == "True":
                self.on()
            else:
                self.off()


# End Output
#######################################################################################################################
