##  Frontend-Imports
import threading
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf


#######################################################################################################################
# Start Input Dialog

class InputDialog(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__("Input-Chooser", transient_for=parent, flags=0)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.set_default_size(200, 300)

        self.mainBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.get_content_area().pack_start(self.mainBox, True, True, 0)
        

        # GPIO
        self.gpioInput = Gtk.Entry()
        self.gpioInput.set_editable(True)
        self.gpioInputLabel = Gtk.Label()
        self.gpioInputLabel.set_label("GPIO-Nummer")
        self.mainBox.pack_start(self.gpioInputLabel, True, True, 1)
        self.mainBox.pack_start(self.gpioInput, True, True, 1)

        # Taster/Schalter/...
        self.inputArt = "taster"
        self.tasterB = Gtk.RadioButton.new_with_label_from_widget(None, "Taster")
        self.tasterB.connect("toggled", self.setArt, "taster")
        self.schalterB = Gtk.RadioButton.new_with_label_from_widget(self.tasterB, "Schalter")
        self.schalterB.connect("toggled", self.setArt, "schalter")
        self.mainBox.pack_start(self.tasterB, True, True, 1)
        self.mainBox.pack_start(self.schalterB, True, True, 1)

        # Prellen
        self.prellCheck = Gtk.CheckButton()
        self.prellCheck.set_label("Prellen")
        self.prellTime = Gtk.Entry()
        self.prellTimeLabel= Gtk.Label()
        self.prellTimeLabel.set_text("Prell-Zeit")
        self.prellTime.set_editable(False)
        self.mainBox.pack_start(self.prellCheck, True, True, 0)
        self.mainBox.pack_start(self.prellTimeLabel, True, True, 0)
        self.mainBox.pack_start(self.prellTime, True, True, 0)

        self.show_all()

    def setArt(self, widget, art):
        self.inputArt = art
        print("setArt:", art, self.inputArt)
    
    def setPrellen(self, widget):
        if self.prellCheck.get_active():
            self.prellTime.set_editable(True)
        else:
            self.prellTime.set_editable(False)

        

# End Input Dialog
#######################################################################################################################

