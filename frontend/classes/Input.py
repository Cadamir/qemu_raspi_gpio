##  Frontend-Imports
import random
#import threading
import time
import gi

from classes.berdav_tcp import VGPIOManager

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

#######################################################################################################################
# Start Input

class Input(Gtk.Frame):
    def __init__(self, nr, inputArt, prellCheck, prellTime, parent):
        super().__init__()

        # Parent
        self.manager = parent.qemuManager
        self.parent = parent

        # Label
        self.nr = nr
        super().set_label(str(self.nr))

        # Input Art
        self.inputArt = inputArt

        # Prell
        self.prell = prellCheck
        self.prellTime = prellTime

        # UI
        switcher = {
            "taster": self.createTaster(),
            "schalter": self.createSchalter()
        }

        print("Input Art:", self.inputArt)
        self.button = switcher.get(self.inputArt, self.createTaster())

        self.add(self.button)
        
    def createTaster(self):
        bt = Gtk.Button()
        bt.set_label("Taster")
        bt.connect("pressed", self.pressed)
        bt.connect("released", self.release)
        return bt
    
    def pressed(self, widget):
        if self.prell:
            self.prellen(1)
        else:
            self.manager.parse("set " + str(self.nr) + " 1")

    def release(self, widget):
        if self.prell:
            self.prellen(0)
        else:
            self.manager.parse("set " + str(self.nr) + " 0")

    def createSchalter(self):
        bt = Gtk.ToggleButton()
        bt.set_label("Schalter")
        bt.connect("toggled", self.changeState)
        return bt

    def changeState(self, widget):
        if self.button.get_active():
            if self.prell:
                self.prellen(1)
            else:
                self.manager.parse("set " + str(self.nr) + " 1")
        else:
            if self.prell:
                self.prellen(0)
            else:
                self.manager.parse("set " + str(self.nr) + " 0")

    def prellen(self, state):
        az = random.choice([3,5,7])
        times = random.choices(range(50,150), k=az)
        for t in times:
            self.manager.parse("set " + str(self.nr) + " " + str(state))
            if state == 1:
                state = 0
            else:
                state = 1
            time.sleep(t/1000)

    def remove(self, widget):
        self.parent.remove(self)


# End Input
#######################################################################################################################
