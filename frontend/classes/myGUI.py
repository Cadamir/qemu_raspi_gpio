from classes.berdav_tcp import VGPIOManager
from classes.Input import Input
from classes.InputDialog import InputDialog
from classes.LedDialog import LedDialog
from classes.Output import Output

##  Frontend-Imports
import threading
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, GLib

#######################################################################################################################
# UI Start
class MyWindow(Gtk.Window):
    ## Init Start
    def __init__(self):
        super().__init__(title="GPIO Overview")
        self.set_default_size(1300, 600)

        ##  Main Box
        self.mainBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(self.mainBox)

        ##  Upper Box
        self.upperBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.mainBox.pack_start(self.upperBox, False, False, 5)

        self.threadButton = Gtk.Button()
        self.threadButton.set_label("Start")
        self.threadButton.connect("clicked", self.startThread)
        self.upperBox.pack_end(self.threadButton, True, True, 3)

        ### GPIOs
        self.gpiosVBox = []
        self.gpiosNr = []
        self.gpiosValue = []
        #self.gpiosState = []
        for x in range(0,32):
            self.gpiosVBox.append(Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=1))
            self.upperBox.pack_start(self.gpiosVBox[x], True, True, 1)
            self.gpiosNr.append(Gtk.Label())
            self.gpiosNr[x].set_label(str(x+1))
            self.gpiosValue.append(Gtk.Label())
            self.gpiosValue[x].set_label("0")
            #self.gpiosState.append(Gtk.Label())
            #self.gpiosState[x].set_label("in")
            self.gpiosVBox[x].pack_start(self.gpiosNr[x], True, True, 0)
            self.gpiosVBox[x].pack_start(self.gpiosValue[x], True, True, 0)
            #self.gpiosVBox[x].pack_start(self.gpiosState[x], True, True, 0)

        ##  Middle Box
        self.middleBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.mainBox.pack_start(self.middleBox, True, True, 5)

        self.middleAddButton = Gtk.Button()
        self.middleAddButton.set_label("+")
        self.middleAddButton.connect("clicked", self.addOutput)
        self.middleBox.pack_end(self.middleAddButton, False, False, 5)

        self.outs = []

        ##  Low Box
        self.lowBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.mainBox.pack_start(self.lowBox, True, True, 5)

        self.inFrames = []
        self.inSwitchs = []
        self.inButton = []
        self.lowAddButton = Gtk.Button()
        self.lowAddButton.set_label("+")
        self.lowAddButton.connect("clicked", self.addInput)
        self.lowBox.pack_end(self.lowAddButton, False, False, 5)

        ##  Footnote
        self.footBox = Gtk.Box(spacing=10)
        self.mainBox.pack_start(self.footBox, False, False, 1)

        ### Source: https://stackoverflow.com/questions/42800482/how-to-set-size-of-a-gtk-image-in-python
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            filename="pictures/logo.png", 
            width=24, 
            height=24, 
            preserve_aspect_ratio=True)
        self.logo = Gtk.Image.new_from_pixbuf(pixbuf)
        self.footBox.pack_start(self.logo, False, False, 0)

        self.qemuManager = VGPIOManager()
        self.loopBool = False
        self.x = threading.Thread(target=self.readThread, daemon=True)
        ## Init End

    ##  Funktionen

    ### Start Thread
    def startThread(self, widget):
        self.x.start()
        #
        #if not self.x.is_alive():
        #    self.threadButton.set_label("Stopp")
        #    self.loopBool = True
        #    self.x = threading.Thread(target=self.readThread, daemon=True)
        #    self.x.start()
        #else:
        #    self.threadButton.set_label("Start")
        #    self.loopBool = False

    ### Add Outputs
    def addOutput(self, widget):
        
        #self.mutex.acquire()

        dialog = LedDialog(self)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            out = Output(int(dialog.gpioInput.get_text()), dialog.color, self)
            self.outs.append(out)
            self.middleBox.pack_start(self.outs[len(self.outs) - 1], False, False, 2)
        
        dialog.destroy()
        self.show_all()
        
        #self.mutex.release()

        
    ### Add Inputs
    def addInput(self, widget):
        
       # self.mutex.acquire()

        dialog = InputDialog(self)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            inp = Input(dialog.gpioInput.get_text(), dialog.inputArt, dialog.prellCheck, dialog.prellTime, self)
            self.lowBox.pack_start(inp, False, False, 2)

        dialog.destroy()
        self.show_all()
        #self.mutex.release()

    ### Toggle In
    def toggleIn(self, button):
        if button.get_active():
            state = "on"
        else:
            state = "off"
        button.set_label(state)

    ### Read GPIO
    def readThread(self):
        while True:
            for x in range(0,31):
                GLib.idle_add(self.idleAkt, x)
            time.sleep(.2)
            self.show_all()
    
    ## IdleFunktion
    def idleAkt(self, x):
        read = self.qemuManager.get(x+1)
        check = "True"
        if (read == check):
            self.gpiosValue[x].set_label("1")
        else:
           self.gpiosValue[x].set_label("0")

# End UI
#######################################################################################################################