##  Frontend-Imports
import threading
import time
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf


#######################################################################################################################
# Start LED Dialog

class LedDialog(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__("LED-Chooser", transient_for=parent, flags=0)
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        self.set_default_size(200, 300)

        # Box Stuff
        self.rightBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.get_content_area().pack_end(self.rightBox, True, True, 0)

        # LED Stuff
        self.gpio = 1
        self.color = "blue"

        # Input Stuff 
        self.gpioInput = Gtk.Entry()
        self.gpioInput.set_editable(True)
        self.gpioInputLabel = Gtk.Label()
        self.gpioInputLabel.set_label("GPIO-Nummer")
        self.get_content_area().pack_start(self.gpioInputLabel, True, True, 1)
        self.get_content_area().pack_start(self.gpioInput, True, True, 0)      

        self.blueB = Gtk.RadioButton.new_with_label_from_widget(None, "Blue")
        self.turquoiseB = Gtk.RadioButton.new_with_label_from_widget(self.blueB, "Turquoise")
        self.greenB = Gtk.RadioButton.new_with_label_from_widget(self.blueB, "Green")
        self.yellowB = Gtk.RadioButton.new_with_label_from_widget(self.blueB, "Yellow")
        self.redB = Gtk.RadioButton.new_with_label_from_widget(self.blueB, "Red")
        self.violettB = Gtk.RadioButton.new_with_label_from_widget(self.blueB, "Violett")

        self.blueB.connect("toggled", self.radioToggle, "blue")
        self.turquoiseB.connect("toggled", self.radioToggle, "turquoise")
        self.greenB.connect("toggled", self.radioToggle, "green")
        self.yellowB.connect("toggled", self.radioToggle, "yellow")
        self.redB.connect("toggled", self.radioToggle, "red")
        self.violettB.connect("toggled", self.radioToggle, "violett")


        self.rightBox.pack_start(self.blueB, True, True, 0)
        self.rightBox.pack_start(self.turquoiseB, True, True, 0)
        self.rightBox.pack_start(self.greenB, True, True, 0)
        self.rightBox.pack_start(self.yellowB, True, True, 0)
        self.rightBox.pack_start(self.redB, True, True, 0)
        self.rightBox.pack_start(self.violettB, True, True, 0)

        self.show_all()

    def radioToggle(self, widget, color):
        self.color = color

# End LED Dialog
#######################################################################################################################
