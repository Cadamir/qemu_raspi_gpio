#!/usr/bin/env python3

from classes import myGUI
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf



#######################################################################################################################
# Main Start

if __name__ == "__main__":
    # my
    win = myGUI.MyWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()

    Gtk.main()

# Main End
#######################################################################################################################
