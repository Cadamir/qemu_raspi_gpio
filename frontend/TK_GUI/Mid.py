import random
from tkinter import *
from TK_GUI import LED_tk

class MidFrame(Frame):
    def __init__(self, parent):
        super().__init__(parent, bg="#CCCCCC")

        self.parent = parent

        self.grid()

        self.outputs = []

        self.addBt = Button(parent, text="Add", command=self.addOutput)
        self.addBt.grid(column=32, row=1)

    def addOutput(self):
        # Variablen
        color = "red"
        nr = 4
        aktTime = .3
        

        # Dialog
        mb = LED_tk.Dialog(self)
        if mb.ok:
            # Dialog nutzen
            color = mb.colorVar.get()
            nr = int(mb.gpioVar.get())
            aktTime = mb.aktTimeVar.get()

            led = LED_tk.LED(self, nr, aktTime, color)
            led.grid(column=len(self.outputs), row=0, padx=5)
            self.outputs.append(led)

    def removeLED(self, nr):
        self.outputs.remove(self)
        for n in range(0, len(self.outputs)-1):
#            self.outputs[nr-1] = self.inputs[nr]
            self.outputs[n].grid(column=(n), row=0)

    def akt(self):
#        print("Länge", len(self.outputs))
        for nr in range(0,len(self.outputs)):
#            print("LED", "get "+str(self.outputs[nr].nr))
            l = self.parent.manager.parse("get "+str(self.outputs[nr].nr))
#            print("l:",l)
            if l == "True":
                self.outputs[nr].on()
            else:
                self.outputs[nr].off()

