from tkinter import *
from TK_GUI import Top, Mid, Bott, Foot
from classes.berdav_tcp import VGPIOManager

#from berdav_tcp import VGPIOManager

class MyWindow(Tk):
    def __init__(self):

        # self init
        super().__init__()
        #self.minsize(800, 400)

        # Berdav TCP
        self.manager = VGPIOManager()

        # Frame init
        self.top = Top.TopFrame(self)
        self.mid = Mid.MidFrame(self)
        self.bott = Bott.BottFrame(self)
        self.foot = Foot.FootFrame(self)

        self.after(10000, self.akt)

    def akt(self):
#        print("after")
        self.top.akt()
        self.mid.akt()
#        self.bott.akt()
        self.after(50, self.akt)

if __name__ == "__main__":
    root = MyWindow()
    root.mainloop()
