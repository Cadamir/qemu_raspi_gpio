from tkinter import *
from tkinter import ttk
from berdav_tcp import VGPIOManager

def startStatus(root, labels, manager):
    print("start")
    for nr in range(1,len(labels)+1):
        labels[nr-1].set(manager.parse("get "+str(nr)))
    root.after(50000, lambda: startStatus(root, labels, manager))

def endStatus(root):
    print("ende")
    root.after_cancel()

qemuManager = VGPIOManager()

root = Tk()

top = ttk.Frame(root, padding=10)
top.grid()
topLable = []

for nr in range(1,32):
    Label(top, text=str(nr)).grid(column=(nr-1), row=0)
    label = StringVar()
    label.set("-")
    Label(top, textvariable=label).grid(column=(nr-1), row=1)
    topLable.append(label)


topStart = ttk.Button(top, text="Start", command=lambda: startStatus(root, topLable, qemuManager)).grid(column=32, row=0)
topEnd = ttk.Button(top, text="Stop", command=lambda: endStatus(root)).grid(column=32, row=1)

mid = ttk.Frame(root, padding=20)
mid.grid()
midbt = ttk.Button(mid, text="LED+").grid(column=30, row=0)

bot = ttk.Frame(root, padding=20)
bot.grid()
botbt = ttk.Button(bot, text="IPut+").grid(column=30, row=0)
#ttk.Label(frm, text="Hello World!").grid(column=0, row=0)
#ttk.Button(frm, text="Quit", command=root.destroy).grid(column=1, row=1)
root.mainloop()