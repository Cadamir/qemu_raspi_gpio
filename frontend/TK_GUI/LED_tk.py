from tkinter import *
from tkinter import ttk
from tkinter import simpledialog
from PIL import Image, ImageTk

class LED(Frame):
    state = True
    def __init__(self, parent, nr, aktTime, color):
        self.parent = parent
        self.nr = nr
        self.aktTime = aktTime
        self.color = color
        self.ok = False

        super().__init__(self.parent)
        self.grid()

        # LED Picture
        switcher = {
            "blue": "LEDblue.png",
            "green": "LEDgreen.png",
            "red": "LEDred.png",
            "turquoise": "LEDturquoise.png",
            "violett": "LEDviolett.png",
            "yellow": "LEDyellow.png"
        }

        self.colorLED = Image.open("pictures/"+switcher.get(self.color, "LEDblue.png"))
        self.greyLED = Image.open("pictures/LEDgrey.png")
        self.colorLED.thumbnail((400,200))
        self.greyLED.thumbnail((400,200))
        self.tkColorLED = ImageTk.PhotoImage(self.colorLED)
        self.tkGreyLED = ImageTk.PhotoImage(self.greyLED)
        self.led = Label(self, image=self.tkColorLED)
        self.led.grid(column=0, row=1, rowspan=5)

        # Überschrift
        self.heading = Label(self, text=str(nr)+" GPIO").grid(column=0, row=0, columnspan=2)

        # Lösch Button
        self.delete = Button(self, text="-", command=self.remove).grid(column=1, row=1, rowspan=5)

        # Bearbeitung Button
        #self.update = Button(self, text="#", command=self.toggle).grid(column=1, row=2)

    def remove(self):
        self.grid_remove()
        self.parent.removeLED(self.nr)

    
    def toggle(self):
        if not self.state:
            self.on()
            self.state = True
        else:
            self.state = False
            self.off()

    def on(self):
        self.led.configure(image=self.tkColorLED)
    
    def off(self):
        self.led.configure(image=self.tkGreyLED)

class Dialog(simpledialog.Dialog):
    def __init__(self, parent):
        super().__init__(parent, title="LED Dialog")

    def body(self, frame):
        self.label = Label(frame, text="GPIO")
        self.label.grid(column=0, row=0, pady=10)

        self.gpioVar = StringVar(frame)
        self.gpioVar.set("1")
        
        self.gpioEntry = Entry(frame, textvariable=self.gpioVar)
        self.gpioEntry.grid(column=1, row=0, columnspan=2, pady=10)

        self.optionList = ["blue", "green", "red", "turquoise", "violett", "yellow"]
        self.colorVar = StringVar(frame)
        self.colorVar.set(self.optionList[0])

        self.colorLabel = Label(frame, text="Color")
        self.colorOptions = OptionMenu(frame, self.colorVar, *self.optionList)
        self.colorOptions.grid(column=1, row=1)


        self.aktTimeLabel = Label(frame, text="Akt. Time")
#        self.aktTimeLabel.grid(column=0, row=2)
        self.aktTimeVar = StringVar(frame)
        self.aktTimeVar.set("0.3")
        self.aktTimeEntry = Entry(frame, textvariable=self.aktTimeVar)
 #       self.aktTimeEntry.grid(column=1, row=2)

        self.bts = Frame(frame)
        self.bts.grid(column=1, row=3)

        self.ok_button = Button(self.bts, text='OK', command=self.ok_pressed)
        self.ok_button.grid(column=0, row=0, padx=10, pady=10)
        cancel_button = Button(self.bts, text='Cancel', command=self.cancel_pressed)
        cancel_button.grid(column=1, row=0, padx=10, pady=10)

        return frame

    def buttonbox(self):
        return None
    
    def ok_pressed(self):
        self.ok = True
        self.destroy()

    def cancel_pressed(self):
        self.ok = False
        self.destroy()


