from tkinter import *

from TK_GUI import LED_tk

from TK_GUI.Input import InputFactory, Schalter, Taster, InputDialog

class BottFrame(Frame):
    def __init__(self, parent):
        super().__init__(parent, bg="#BBBBBB")

        self.parent = parent

        self.grid()

        self.inputs = []

        self.addBt = Button(parent, text="Add", command=self.addInput)
        self.addBt.grid(column=32, row=2)

    def addInput(self):
        # Dialog
        mb = InputDialog(self)
        if mb.ok:
            inP = InputFactory().build(self, mb.nameVar.get(), mb.nrVar.get(), mb.prellVar.get())
            inP.grid(column=len(self.inputs), row=0, padx=5)
            self.inputs.append(inP)

    def removeInP(self, nr):
        self.inputs.remove(self)
        for n in range(0, len(self.inputs)-1):
#            self.inputs[nr-1] = self.inputs[nr]
            self.inputs[n].grid(column=(n), row=0)

    def akt(self):
        print("")

        
