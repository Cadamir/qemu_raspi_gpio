
# QEMU_RASPI_GPIO

## Idee
Dieses Repository soll Studenten die Möglichkeiten bieten, einen Raspberry Pi (RasPi) zu nutzen ohne ein RasPi kaufen zu müssen. 
Oder das Testen an einem RasPi soll vereinfacht werden. 
Dabei wird QEMU genutzt um den RasPi zu emulieren. 
Mit Python wird eine GUI dargestellt, welche den Zugriff auf GPIOs ermöglicht. 

## Nutzung
Um das Repository nutzen zu können, sind folgende Schritte notwendig.

* Download und Installation von QEMU 6.1.0 oder neuer ([Download](https://gitlab.com/Cadamir/qemu_raspi_gpio/-/wikis/Wie-l%C3%A4dt-man-QEMU-herunter))
* Download Repository ([Download](https://gitlab.com/Cadamir/qemu_raspi_gpio/-/archive/main/qemu_raspi_gpio-main.zip))
* Download Raspbios Bullseye Image (und entpacken in das `./image`-Verzeichnis, **2021-10-30-raspios-bullseye-armhf-lite.img**, [Download](https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip))
* Entpacken der Kernel-Datei und der "device tree blob" (dtb) Datei (und korrekte Speicherung in ihren Verzeichnissen)
  * `*.dtb` in das `./dtb`-Verzeichnis
  * `kernel8.img` in das `./kernel`-Verzeichnis

Wenn diese Schritte durchgeführt wurden, muss das Raspberry Pi Image noch entsprechend angepasst werden:
* "Kernel-Update" unterbinden (Pflicht, sonst geht es kaputt)
* Passwort ändern
* `ssh` aktivieren
* Speichergröße anpassen
* `lightpd` installieren
* `libgpiod` installieren
* `node.js` installieren

Um die GPIOs zu nutzen, ist noch Python notwendig mit folgenden Paketen:
* python3
* Tkinter
* PIL (Python Image Libary)
* Socket

Es werden zwei Terminals/Kommandozeilen benötigt. 
Im ersten wird im `./frontend`-Verzeichnis der Befehl `./main.py` ausgeführt. 
Es sollte nichts passieren, außer das keine weitere Eingabe möglich ist.
Im Hintergrund wurde ein Socket geöffnet und es wird gewartet bis sich ein Client an den Socket (localhost:2222) anbindet.

Im zweiten Terminal wird im Hauptordner der Befehl `./run/run_tcp.sh` (Für Windows `./run/run_tcp.bat`) ausgeführt. 
Dadurch wird der RasPi gestartet. 
Zeitgleich wird die Grafische Oberfläche gestartet.
Der Raspberry Pi bzw. QEMU hat sich mit dem Socket verbunden.
Dadurch konnte der weitere Programmcode von `./main_tk.py` ausgeführt werden.

![frontend.png](https://gitlab.com/Cadamir/qemu_raspi_gpio/-/raw/main/meta/Anleitung%20Bilder/frontend.png)

## Quellen:
Es wurde Quellcode von [berdav qemu-rpi-gpio](https://github.com/berdav/qemu-rpi-gpio), [wzab BR_Internet_Radio](https://github.com/wzab/BR_Internet_Radio) und [QEMU](https://github.com/qemu/qemu) verwendet.
